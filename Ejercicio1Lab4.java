import java.util.Scanner; 
public class Ejercicio1Lab4 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        float notas[],inter[];
        float promedio=0;
        float menor=1000;
        int intermedio=0; 
        notas = new float[10];
        inter = new float[10];
        System.out.println("=====================================");
        System.out.println("");
        System.out.println("Digite las notas de los estudiantes: ");
        for(int i=0;i<10;i++){
            System.out.print("Nota "+(i+1)+": ");
            notas[i]= teclado.nextFloat();
            promedio += notas[i];
        }
        promedio = promedio/10;
        System.out.println("El promedio de las notas es de: "+promedio);
        for (int k=0; k<10;k++){
            inter[k] = Math.abs(promedio-notas[k]);
            if (inter[k]<menor){
                intermedio = k;
            }
    
        }
        System.out.println("El valor intermedio de las notas fue de: "+notas[intermedio]);
        System.out.println("");
        System.out.println("=====================================");


    }
}