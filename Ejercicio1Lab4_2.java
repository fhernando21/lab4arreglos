import java.util.Scanner; 
public class Ejercicio1Lab4_2 {
    public static void main(String[] args) {
        int f,c,i,j;
        Scanner teclado = new Scanner(System.in);
        int matriz[][];
        int matriz2[][];
        do {
        System.out.println("Digite el numero de filas: ");
        f = teclado.nextInt();
        System.out.println("Digite el numero de columnas: ");
        c = teclado.nextInt();
        matriz = new int [f][c];
        matriz2 = new int [c][f];
        } while (f<=0 || c<=0);
        System.out.println("Digite los elementos de la matriz: ");
        for (i=0;i<f;i++){
            for ( j=0;j<c;j++ ){
                System.out.print("["+(i+1)+","+(j+1)+"]: ");
                matriz [i][j] = teclado.nextInt();

            }
            
        }
        for ( i=0;i<c;i++){
            for ( j=0;j<f;j++ ){

                matriz2[i][j] = matriz[j][i];

            }
        }
        System.out.println("La matriz original es: ");
        for (i=0;i<f;i++){
            System.out.print("|| ");
            for ( j=0;j<c;j++ ){
                if (matriz[i][j]>=0){
                if ((Math.abs(matriz[i][j]))>=10000){
                System.out.print("  "+matriz[i][j]+"");
                }
                else{
                    if ((Math.abs(matriz[i][j]))>=1000){
                        System.out.print("  "+matriz[i][j]+" ");
                    }
                    else {
                        if ((Math.abs(matriz[i][j]))>=100){
                            System.out.print("  "+matriz[i][j]+"  ");
                        }
                        else {
                            if ((Math.abs(matriz[i][j]))>=10){
                                System.out.print("  "+matriz[i][j]+"   ");
                            }
                            else {
                                System.out.print("  "+matriz[i][j]+"    ");
                               
                            } 
                            
                           
                        
                    }
                    
                }
            }
        }
        else {
            if ((Math.abs(matriz[i][j]))>=10000){
                System.out.print(" "+matriz[i][j]+"");
                }
                else{
                    if ((Math.abs(matriz[i][j]))>=1000){
                        System.out.print(" "+matriz[i][j]+" ");
                    }
                    else {
                        if ((Math.abs(matriz[i][j]))>=100){
                            System.out.print(" "+matriz[i][j]+"  ");
                        }
                        else {
                            if ((Math.abs(matriz[i][j]))>=10){
                                System.out.print(" "+matriz[i][j]+"   ");
                            }
                            else {
                                System.out.print(" "+matriz[i][j]+"    ");
                               
                            } 
                            
                           
                        
                    }
                    
                }
            }
        }
        }

            System.out.println(" ||");
        }
        System.out.println("");
        System.out.println("La matriz transpuesta es: ");
        for ( i=0;i<c;i++){
            System.out.print("|| ");
            for ( j=0;j<f;j++ ){
                if (matriz2[i][j]>=0){
                if ((Math.abs(matriz2[i][j]))>=10000){
                System.out.print("  "+matriz2[i][j]+"");
                }
                else{
                    if ((Math.abs(matriz2[i][j]))>=1000){
                        System.out.print("  "+matriz2[i][j]+" ");
                    }
                    else {
                        if ((Math.abs(matriz2[i][j]))>=100){
                            System.out.print("  "+matriz2[i][j]+"  ");
                        }
                        else {
                            if ((Math.abs(matriz2[i][j]))>=10){
                                System.out.print("  "+matriz2[i][j]+"   ");
                            }
                            else {
                                System.out.print("  "+matriz2[i][j]+"    ");
                               
                            } 
                            
                           
                        
                    }
                    
                }
            }
        }
        else {
            if ((Math.abs(matriz2[i][j]))>10000){
                System.out.print(" "+matriz2[i][j]+"");
                }
                else{
                    if ((Math.abs(matriz2[i][j]))>=1000){
                        System.out.print(" "+matriz2[i][j]+" ");
                    }
                    else {
                        if ((Math.abs(matriz2[i][j]))>=100){
                            System.out.print(" "+matriz2[i][j]+"  ");
                        }
                        else {
                            if ((Math.abs(matriz2[i][j]))>=10){
                                System.out.print(" "+matriz2[i][j]+"   ");
                            }
                            else {
                                System.out.print(" "+matriz2[i][j]+"    ");
                               
                            } 
                            
                           
                        
                    }
                    
                }
            }

        }
        }

            System.out.println(" ||");
        }
        System.out.println("");
    }
}
