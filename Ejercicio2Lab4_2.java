import java.util.Scanner;
public class Ejercicio2Lab4_2 {
    
    public static void main(String[] args) {
        int matriz[][], f, c, n,sumap=0,suma=0;
        Scanner teclado = new Scanner(System.in);
        System.out.println("El numero de valores enteros de la matriz es: ");
        do {

            n = teclado.nextInt();
            if (Math.sqrt(n) == (int)Math.sqrt(n) || n>=1){

            } 
            else {
            System.out.println("No es posible convertir a una matriz cuadrada.");
            }

        } while (Math.sqrt(n) != (int)Math.sqrt(n) || n<=0);
        f = (int)Math.sqrt(n);
        c = (int)Math.sqrt(n);
        matriz = new int [f][c];
        System.out.println("Digite los elementos de la matriz: ");
        for (int i=0; i<f;i++){
            for (int j=0; j<c; j++){
                System.out.print("["+(i+1)+","+(j+1)+"]= ");
                matriz [i][j]= teclado.nextInt();
                if (i>j){
                    sumap=sumap+matriz[i][j];
                }
                if (j>i){
                    suma=suma+matriz[i][j];
                }
            }
        }

        System.out.println("La matriz original queda: ");
        for (int i=0;i<f;i++){
            System.out.print("|| ");
            for ( int j=0;j<c;j++ ){
                if (matriz[i][j]>=0){
                if ((Math.abs(matriz[i][j]))>=10000){
                System.out.print("  "+matriz[i][j]+"");
                }
                else{
                    if ((Math.abs(matriz[i][j]))>=1000){
                        System.out.print("  "+matriz[i][j]+" ");
                    }
                    else {
                        if ((Math.abs(matriz[i][j]))>=100){
                            System.out.print("  "+matriz[i][j]+"  ");
                        }
                        else {
                            if ((Math.abs(matriz[i][j]))>=10){
                                System.out.print("  "+matriz[i][j]+"   ");
                            }
                            else {
                                System.out.print("  "+matriz[i][j]+"    ");
                               
                            } 
                            
                           
                        
                    }
                    
                }
            }
        }
        else {
            if ((Math.abs(matriz[i][j]))>=10000){
                System.out.print(" "+matriz[i][j]+"");
                }
                else{
                    if ((Math.abs(matriz[i][j]))>=1000){
                        System.out.print(" "+matriz[i][j]+" ");
                    }
                    else {
                        if ((Math.abs(matriz[i][j]))>=100){
                            System.out.print(" "+matriz[i][j]+"  ");
                        }
                        else {
                            if ((Math.abs(matriz[i][j]))>=10){
                                System.out.print(" "+matriz[i][j]+"   ");
                            }
                            else {
                                System.out.print(" "+matriz[i][j]+"    ");
                               
                            } 
                            
                           
                        
                    }
                    
                }
            }
        }
        }

            System.out.println(" ||");
        }

        System.out.println("La suma de los elementos por encima de la diagonal principal es: "+sumap);
        System.out.println("La suma de los elementos por debajo de la diagonal principal es: "+suma);
        
    }
}
