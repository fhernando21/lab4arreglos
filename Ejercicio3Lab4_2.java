import java.util.Scanner;

public class Ejercicio3Lab4_2 {
    
    public static void main(String[] args) {
        int matriz[][],k=0,v[];
        int dp=0,ds=0,det1=0,det2=0,det3=0,determinante;
        matriz = new int [3][3];
        v = new int [9];
        Scanner teclado = new Scanner(System.in);
        for (int i=0; i<3 ; i++){
            for (int j=0; j<3 ;j++){
                System.out.print("["+(i+1)+","+(j+1)+"]: ");
                matriz[i][j]= teclado.nextInt();
                v[k] = matriz[i][j];
                k=k+1;
            }
        } 
        for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                if(i==j){
                    dp= dp+ matriz[i][j];
                }
                if ((i+j)==2){
                    ds = ds+ matriz[i][j];
                }
            }
        }
        det1 = dp-ds;
        dp=0;ds=0;
        for (int i=0;i<3;i++){
            matriz[0][i] = v[(i+3)];
        }
        for (int i=0;i<3;i++){
            matriz[1][i] = v[(i+6)];
        }
        for (int i=0;i<3;i++){
            matriz[2][i] = v[(i)];
        }
        for (int i=0;i<2;i++){
            for (int j=0;j<3;j++){
                if(i==j){
                    dp= dp+ matriz[i][j];
                }
                if ((i+j)==2){
                    ds = ds+ matriz[i][j];
                }
            }
        }
        det2=dp-ds;
        dp=0;ds=0;
        for (int i=0;i<3;i++){
            matriz[0][i] = v[(i+6)];
        }
        for (int i=0;i<3;i++){
            matriz[1][i] = v[(i)];
        }
        for (int i=0;i<3;i++){
            matriz[2][i] = v[(i+3)];
        }
        for (int i=0;i<2;i++){
            for (int j=0;j<3;j++){
                if(i==j){
                    dp = dp+ matriz[i][j];
                }
                if ((i+j)==2){
                    ds = ds+ matriz[i][j];
                }
            }
        }
    
        det3=dp-ds;
        determinante= det1+det2+det3;
        System.out.println("El determinante de la matriz es: "+determinante);

        

        
    } 
}
